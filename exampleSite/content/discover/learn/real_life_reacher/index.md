+++
date = "2020-10-19T14:09:57+01:00"
publishdate = "2020-10-19T14:09:57+01:00"
authors = ["phil-winder"]
title = "Real Life Reacher with the PPO Algorithm"

description = "Investigating PPO with a simplified real-life reacher robot."

supplementary_materials = ["Chapter 6. Beyond Policy Gradients"]

supplementary_materials_weight = 12

tags = ["PPO", "Robotics"]

keywords = ["Policy Gradients", "PG methods", "reinforcement learning", "rl", "PPO", "reacher", "servo"]

includes_math = false

[image]
    src = "images/moves.gif"
+++

Reacher is [an old Gym environment](https://gym.openai.com/envs/Reacher-v2/) that simulates an arm that is asked to _reach_ for a coordinate. In this example I have created a simplified real-life version of this environment using servo motors and used PPO to train a policy.

{{< figure src="images/moves.gif" >}}

There's a bit too much code to go in a notebook, so I have decided to present this example as a walk-through instead. All of the code is located in [a separate repository](https://gitlab.com/winderresearch/rl/projects/real-life-reacher).

### Professional Help

If you're doing something like this in your business, then please reach out to [https://WinderResearch.com](WinderResearch). Our experts can provide no-nonsense help that I guarantee will save you time.

## The Easy Route: Simulation

So nobody is left out, I created a [simple simulation of the hardware](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/gym_real_life_reacher/envs/controller.py#L18) so you can run the experiment without any electronics.

### Installation

I used `stable_baselines` 2 for the PPO implementation, which unfortunately means you have to use `Tensorflow` 1.5, which means you have to use Python 3.7. Install the project by cloning the repository and [using poetry](https://python-poetry.org/) to install the dependencies.

1. git clone https://gitlab.com/winderresearch/rl/projects/real-life-reacher
2. cd real-life-reacher && poetry install

### Training

Train the agent by instantiating the environment with the simulated controller.

```sh
python ppo_policy.py --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42 --model_path trained_policies/ppo_RealLifeReacher-v0_sim.zip
```

This will load the [PPO hyperparameters](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/hyperparams/ppo.yaml) train the agent, store the results in the `./monitoring` directory and save the trained parameters `trained_policies/ppo_RealLifeReacher-v0_sim.zip`.

### Evaluation

You can then reuse the policy and/or re-evaluate it again with:

```sh
python ppo_policy.py trained_policies/ppo_RealLifeReacher-v0_sim.zip --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42
```

This will spit out an interesting plot that shows you the policy. This is only possible because of small state/action space.

{{< img src="images/policy_random_goal.png" >}}

The y-axis shows the observation from the (simulated) servo feedback. The x-axis shows the goal selected by the environment. The shaded area is the policy for each goal-observation position; the action. Light values are negative, dark values are positive.

In this super simple scenario, you would expect that there should be a gradient going from left to right, because the goal corresponds exactly to the action. If you tell it to move to -0.5, the action should aways be -0.5.

But interestingly you can see that there is often a subtle slope. I think this is purely due to the policy model used by PPO. The model is over determined for the complexity of the problem. I suspect that with further tweaking of the various hyper-parameters you could get it to converge.

### Hyper-Parameter Tuning

For hyper-parameter tuning I used the `optuna` library. You can [see the code in the repository](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/tune_ppo.py) if you're interested.

## Real Life!

Now for the interesting part. It is easy to extend this into the real world thanks to the [controller interface](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/gym_real_life_reacher/envs/controller.py). But you will need some hardware.

- 1x LX-16A servo motors
- 1x BusLinker V2.4 serial servo driver board
- An extra power supply to power the motors via the driver board
- A USB cable

You should be able to find these on Amazon or in your favourite online store.

The [code for the real life controller](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/gym_real_life_reacher/envs/controller.py#L32) converts the requests from the +/- 1 to commands that the servo can understand. There's also [a bit of code](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/gym_real_life_reacher/wrappers.py#L4) to prevent episodes going on for too long. Sometimes the agent got itself stuck due to some unlucky parameters.

The reset mechanism is interesting too. I chose to [randomly reset the controller](https://gitlab.com/winderresearch/rl/projects/real-life-reacher/-/blob/master/gym_real_life_reacher/envs/real_life_reacher_v0.py#L68) to any position, not to a fixed position. That way the agent is able to generalize to any position.

### Training and Evaluation

The instructions/code is exactly the same as before, except for the addition of a `--for_real` flag.

So for example, to train a new policy you would run:

```sh
python ppo_policy.py --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42 --model_path trained_policies/ppo_RealLifeReacher-v0_sim.zip --for_real
```

And to evaluate and/or reuse an trained policy you would run:

```sh
python ppo_policy.py trained_policies/ppo_RealLifeReacher-v0_real.zip --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42 --for_real
```

## Future Work

Cool eh! Obviously this is ridiculously simple in the grand scheme of things. You could imagine a much more complex arm, where the challenge is to pick up an object from any position or draw a picture or play a tune. Anything you can think of.

And in an industrial project you need to think about all the other elements, operational deployment, safety, robustness, etc. That's a much bigger project. If you're doing something like this in your business, then please reach out to [https://WinderResearch.com](WinderResearch). Our experts can provide no-nonsense help that I guarantee will save you time.